var ready = (callback) => {
    if (document.readyState != "loading") callback();
    else document.addEventListener("DOMContentLoaded", callback);
  }
  
  ready(() => { 
      /* Do things after DOM has fully loaded */ 
      makeplot();
  });

function makeplot() {
    var stock = document.getElementById("stock-select");
    var myOption = stock.options[stock.selectedIndex].getAttribute('on');
    if(myOption == "1"){var path = "includes/data/AAPL.csv"}
    if(myOption == "2"){var path = "includes/data/GME.csv"}
    if(myOption == "3"){var path = "includes/data/SPY.csv"}
    if(myOption == "4"){var path = "includes/data/TSLA.csv"}
    console.log("makeplot: start")
    fetch(path)
    .then((response) => response.text()) /* asynchronous */
    .catch(error => {
        alert(error)
         })
    .then((text) => {
      console.log("csv: start")
      csv().fromString(text).then(processData)  /* asynchronous */
      console.log("csv: end")
    })
    console.log("makeplot: end")
};

function processData(data) {
  console.log("processData: start")
  let x = [], y = []

  for (let i=0; i<data.length; i++) {
      row = data[i];
      x.push( row['Date'] );
      y.push( row['Close'] );
  } 
  makePlotly( x, y );
  console.log("processData: end")
}

function makePlotly( x, y ){
  var stock = document.getElementById("stock-select");
  var myOption = stock.options[stock.selectedIndex].getAttribute('on');
  console.log("makePlotly: start")
  var traces = [{
        x: x,
        y: y
  }];
  if(myOption == "1"){
    var layout  = { title: "Apple Stock Price History"}}
  if(myOption == "2"){
    var layout  = { title: "GameStop Stock Price History"}}
  if(myOption == "3"){
    var layout  = { title: "Tesla Stock Price History"}}
  if(myOption == "4"){
    var layout  = { title: "S&P500 Stock Price History"}}
  myDiv = document.getElementById('myDiv');
  Plotly.newPlot( myDiv, traces, layout );
  console.log("makePlotly: end")
};